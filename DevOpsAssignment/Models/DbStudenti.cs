namespace DevOpsAssignment.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DbStudenti : DbContext
    {
        public DbStudenti()
            : base("name=DbStudenti")
        {
        }

        public virtual DbSet<Esami> Esamis { get; set; }
        public virtual DbSet<Studenti> Studentis { get; set; }
        public virtual DbSet<Voti> Votis { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Esami>()
                .Property(e => e.Nome)
                .IsFixedLength();

            modelBuilder.Entity<Esami>()
                .HasMany(e => e.Votis)
                .WithRequired(e => e.Esami)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Studenti>()
                .Property(e => e.Nome)
                .IsFixedLength();

            modelBuilder.Entity<Studenti>()
                .Property(e => e.Cognome)
                .IsFixedLength();

            modelBuilder.Entity<Studenti>()
                .Property(e => e.Matricola)
                .IsFixedLength();

            modelBuilder.Entity<Studenti>()
                .HasMany(e => e.Votis)
                .WithRequired(e => e.Studenti)
                .WillCascadeOnDelete(false);
        }
    }
}
