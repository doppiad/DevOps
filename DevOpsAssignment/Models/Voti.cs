namespace DevOpsAssignment.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Voti")]
    public partial class Voti
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdVoto { get; set; }

        public int IdStudente { get; set; }

        public int IdEsame { get; set; }

        public int Valutazione { get; set; }

        public virtual Esami Esami { get; set; }

        public virtual Studenti Studenti { get; set; }
    }
}
