﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DevOpsAssignment.Models;

namespace DevOpsAssignment.Controllers
{
    public class VotisController : Controller
    {
        private DbStudenti db = new DbStudenti();

        // GET: Votis
        public ActionResult Index()
        {
            var votis = db.Votis.Include(v => v.Esami).Include(v => v.Studenti);
            return View(votis.ToList());
        }

        // GET: Votis/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Voti voti = db.Votis.Find(id);
            if (voti == null)
            {
                return HttpNotFound();
            }
            return View(voti);
        }

        // GET: Votis/Create
        public ActionResult Create()
        {
            ViewBag.IdEsame = new SelectList(db.Esamis, "IdEsame", "Nome");
            ViewBag.IdStudente = new SelectList(db.Studentis, "IdStudente", "Nome");
            return View();
        }

        // POST: Votis/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdVoto,IdStudente,IdEsame,Valutazione")] Voti voti)
        {
            if (ModelState.IsValid)
            {
                db.Votis.Add(voti);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdEsame = new SelectList(db.Esamis, "IdEsame", "Nome", voti.IdEsame);
            ViewBag.IdStudente = new SelectList(db.Studentis, "IdStudente", "Nome", voti.IdStudente);
            return View(voti);
        }

        // GET: Votis/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Voti voti = db.Votis.Find(id);
            if (voti == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdEsame = new SelectList(db.Esamis, "IdEsame", "Nome", voti.IdEsame);
            ViewBag.IdStudente = new SelectList(db.Studentis, "IdStudente", "Nome", voti.IdStudente);
            return View(voti);
        }

        // POST: Votis/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdVoto,IdStudente,IdEsame,Valutazione")] Voti voti)
        {
            if (ModelState.IsValid)
            {
                db.Entry(voti).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdEsame = new SelectList(db.Esamis, "IdEsame", "Nome", voti.IdEsame);
            ViewBag.IdStudente = new SelectList(db.Studentis, "IdStudente", "Nome", voti.IdStudente);
            return View(voti);
        }

        // GET: Votis/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Voti voti = db.Votis.Find(id);
            if (voti == null)
            {
                return HttpNotFound();
            }
            return View(voti);
        }

        // POST: Votis/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Voti voti = db.Votis.Find(id);
            db.Votis.Remove(voti);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
