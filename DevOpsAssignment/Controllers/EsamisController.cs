﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DevOpsAssignment.Models;

namespace DevOpsAssignment.Controllers
{
    public class EsamisController : Controller
    {
        private DbStudenti db = new DbStudenti();

        // GET: Esamis
        public ActionResult Index()
        {
            return View(db.Esamis.ToList());
        }

        // GET: Esamis/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Esami esami = db.Esamis.Find(id);
            if (esami == null)
            {
                return HttpNotFound();
            }
            return View(esami);
        }

        // GET: Esamis/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Esamis/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdEsame,Nome,CFU")] Esami esami)
        {
            if (ModelState.IsValid)
            {
                db.Esamis.Add(esami);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(esami);
        }

        // GET: Esamis/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Esami esami = db.Esamis.Find(id);
            if (esami == null)
            {
                return HttpNotFound();
            }
            return View(esami);
        }

        // POST: Esamis/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdEsame,Nome,CFU")] Esami esami)
        {
            if (ModelState.IsValid)
            {
                db.Entry(esami).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(esami);
        }

        // GET: Esamis/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Esami esami = db.Esamis.Find(id);
            if (esami == null)
            {
                return HttpNotFound();
            }
            return View(esami);
        }

        // POST: Esamis/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Esami esami = db.Esamis.Find(id);
            db.Esamis.Remove(esami);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
