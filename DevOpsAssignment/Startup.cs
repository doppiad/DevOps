﻿using DevOpsAssignment.Models;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DevOpsAssignment.Startup))]
namespace DevOpsAssignment
{
    public partial class Startup
    {
        public Startup()
        {
            var db = new DbStudenti();
            if (!db.Database.Exists())
                db.Database.Create();
        }
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
