Membri del gruppo:
1)	Ditolve Davide 806953

App distribuita con database distribuito.

Per lo svolgimento di questo assignement, ho utilizzato visual studio per la creazione di un sito web dinamico basato sul net framework e linguaggio C#. Per la distribuzione del servizio ho deciso di appoggiarmi a un cloud service ovvero Azure (piattaforma di cloud di microsoft)
Per la prima versione il sito web sarà molto semplice, consentirà ai professori di registrarsi e dopo essersi loggati, aggiungere esami, studenti e votazioni agli stessi.
Tutti i dati utenti saranno archiviati su due DB distribuiti hostato sempre su Azure e l’altro in una vm dedicata. 
Sul primo saranno messi gli account utenti sul secondo i dati dell’applicazione
La struttura è molto semplice e consente agli utenti di inserire una anagrafica di base e di appartenere a uno o più ruoli.
La grafica è basata su bootstrap 4 e l’accesso al DB è effettuato tramite linq e entity framework 6 tramite pattern DAO.
Il layer dati che gestisce l’interfaccia DB-Software è contenuto all’interno della cartella model.
Ho scelto .Net in quanto semplice da interfacciare con Visual Studio 2017 e i servizi azure, inoltre è il linguaggio che meglio padroneggio in quanto lavoro con questi strumenti da oltre 3 anni.
Dopo averlo containerizzato tramite Docker, ho deployato l’immagine tramite servizio app di Azure al seguente indirizzo: https://devopsassignment.azurewebsites.net (l’immagine è windows-based non è possibile compilare app .netframework come container linux, per fare questo è necessario usare una versione diversa di .net ovvero .netcore)
Infine ho caricato il tutto su Azure (repository git, devops, db e docker file) e clonato il tutto su GitLab (per rispettare la richiesta) con il mirroring continuo.

I punti dell'assignement che sono stati analizzati sono:
1) Provisoning effettuato tramite Docker
4) Continuous Integration/Continuous Development (CI/CD) tramite Azure devops per la distribuzione continua attraverso la costruzione di una pipeline e tramite GitLab usando il Container Registry per la distribuzione continua dell’immagine docker. Al fine di sfruttare al meglio la CI ho creato un progetto di unit test all’interno della soluzione C# con diversi metodi di test, la CI verifica che tutti i test compilino e passino correttamente prima di committare effettivamente il codice nel repository
5) Monitoring tramite Insight, dashboard accessibile solo tramite account personale su portale azure, che permette di avere una panoramica completa che mette in correlazione il codice eseguito, le chiamate a db effettuate con le interazioni utente sul frontend
L'app è sempre disponibile e la si può raggiungere all'indirizzo: https://devopsassignment.azurewebsites.net
La repository GIT azure è disponibile aperta al pubblico al seguente indirizzo:
https://davideditolve.visualstudio.com/DevOpsAssignment2
La repository GIT LAB è disponibile al seguente indirizzo:
https://gitlab.com/doppiad/DevOps
È possibile testare l’app in locale sulla porta 50366 tramite il seguente comando:
docker run registry.gitlab.com/doppiad/devops:latest
